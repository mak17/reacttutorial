import React, { useMemo, useState } from 'react';
import Counter from './Counter';
import CounterWithHook from './CounterWithHook';

const UseMemo = () => {
    const [firstCounter, setFirstCounter] = useState(0);
    const [secondCounter, setSecondCounter] = useState(0);
    const [point, setPoint] = useState({x: 0, y: 10});


    const increaseFirstCounter = () => setFirstCounter(firstCounter + 1);
    const increaseSecondCounter = () => setSecondCounter(secondCounter + 1);
    const changePointX = () => setPoint({...point, x: point.x+1});
    const changePointY = () => setPoint({...point, y: point.y+1});

    /**
     * Use memo hook    
     * In dependecy table we can variable (Cahnge the varialbe rerende the component)
     * 
     * WOrks both methods
     */
    const CounterWithHookComponent = useMemo(() => 
        <CounterWithHook />, [firstCounter, point.x]
    );

    /**
     * const CounterWithHookComponent = useMemo(() => 
     *  <CounterWithHook />, [firstCounter, point]
     *  );
     * 
     */

    return(
        <div>
           <h1>First counter: {firstCounter}</h1>
           <h1>Second counter: {secondCounter}</h1>
           <Counter />
           {CounterWithHookComponent}
           <button onClick={increaseFirstCounter}>Increase first counter</button>
           <button onClick={increaseSecondCounter}>Increase second counter</button>
           <button onClick={changePointX}>Change X</button>
           <button onClick={changePointY}>Change Y</button>
        </div>
    )
}

export default UseMemo