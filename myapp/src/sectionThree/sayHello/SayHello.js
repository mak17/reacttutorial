import React, { Component } from 'react';

class SayHello extends Component {
  state = {
      nameFromState: 'Marcin',
      messageFromState: 'Hello'
  }

  render() {
    const { nameFromState, messageFromState } = this.state 
    const { nameFromProps } = this.props
    return (
      <div>
        <h1>{messageFromState} {nameFromState} from class!!!!!</h1>
        <h1>Name from props: {nameFromProps}</h1>
      </div>
    )
  }
}

export default SayHello;
