import React, { Component } from 'react';
import Item from './Item';

class Items extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { items, handleShoppingList, isInShoppingList } = this.props;
        const itemsComponent = items.map(item => <Item key={item.id} item={item} handleShoppingList={handleShoppingList} isInShoppingList={isInShoppingList} />);
        return (
            <>
                <h1>Order list:</h1>
                {itemsComponent}
            </>
        )
    }
}

export default Items