import React, { Component } from 'react';

const PersonComponent = ({person, deletePerson}) => {
    return <li>{person.name} <button onClick={() => deletePerson(person.id)} >Delete person</button></li>
}
    
class Person extends Component {
    constructor(props){
        super(props)

        this.state = {
            persons: [
                {
                    name: 'Person 1',
                    id: 0
                },
                {
                    name: 'Person 2',
                    id: 1
                },
                {
                    name: 'Person 3',
                    id: 2
                },
                {
                    name: 'Person 4',
                    id: 3
                },
            ]
        }

        this.deletePerson = this.deletePerson.bind(this)
    }

    deletePerson(id) {
        let personList = [...this.state.persons];
        const person = personList.find(person => person.id ===id);
        
        const indexPerson = personList.indexOf(person);

        personList.splice(indexPerson,1);
        this.setState({
            persons: personList
        });
    }

    render() {
        const { persons } = this.state;
        return(
            <ul>
                { persons.map(person => <PersonComponent person={person} deletePerson={this.deletePerson}/>) }
            </ul>
        )
    };
}

export default Person