import React from 'react';

const Counter = ({callBack}) => {
    console.log("render from first counter");

    return(
        <button onClick={callBack}>First counter</button>
    )
}

export default React.memo(Counter)