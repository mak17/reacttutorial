import React, { Component } from 'react';

class Task extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { task, deleteTask, finishTask } = this.props
        return (
            <div style={{border: "2px solid blue", margin: "20px"}}>
                <h5>
                    <span style={{color: task.important && task.active ? "red" : "black"}} >
                        {task.text}
                    </span>
                </h5> 
            {finishTask && <button onClick={() => finishTask(task.id)}>End task</button>}
            {deleteTask && <button onClick={() => deleteTask(task.id)}>Remove task</button>}
        </div>
        )
    }
}

export default Task