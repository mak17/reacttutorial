import React, { Component } from 'react';

class Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
            inputOneValue: 'Test',
            checkedValue: false,
            selectValue: 'Option 3'
        }

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value});
    }

    render() {
        const { inputOneValue, checkedValue, selectValue } = this.state;
        return (
            <div>
                <form>
                    <label>
                        Name:
                        <input type="text" name="name" />
                    </label>
                    <br />
                    <label>
                        Email:
                        <input type="email" name="email" />
                    </label>
                    <br />
                    <button>Save</button>
                </form>
                
                <h1>--------------------------</h1>

                <label>
                    Input 1 
                    <input type="text" name="inputOneValue" value={inputOneValue} onChange={this.handleChange}/>
                </label>
                <br />
                <label>
                    Check 1 
                    <input type="checkbox" name="checkedValue" checked={checkedValue} onChange={this.handleChange} />
                </label>
                <br />
                <label>
                    Select One 
                    <select name="selectValue" value={selectValue} onChange={this.handleChange}>
                        <option value="Option 1">Option 1</option>
                        <option value="Option 2">Option 2</option>
                        <option value="Option 3">Option 3</option>
                        <option value="Option 4">Option 4</option>
                    </select>
                </label>
            </div>
        )
    }
}

export default Form