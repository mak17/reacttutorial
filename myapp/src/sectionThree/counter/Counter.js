import React, { Component } from 'react';

class Counter extends Component {

    state = {
        result: 0,
        counter: 0
    }

    handleClick(number){
        this.setState({
            counter: this.state.counter + 1,
            result: this.state.result + number
        });
      
    }

    clearResult = () => {
        this.setState({result: 0});
    }

    render() {
        /**
         * destructuring
         * Allow to get only data from the object which we want
         * Code is clearly
         * Allow to use directly variables from object
         * Example:
         *  tab = ['1','2','3','4'];
         *  const { test1, ...test } = tab;
         *  test1 -> 1
         *  test -> ['2','3','4']
         */
        const { result , counter } = this.state;
        return (
            <div>
                <button onClick={() => this.handleClick(1)}>+ 1</button>
                <button onClick={() => this.handleClick(-1)}>- 1</button>
                <button onClick={this.clearResult}>Reset result</button>

                <h1>Counter: {counter}</h1>
                <h1>Result: {result}</h1>
            </div>
        
        )
    }
}

export default Counter