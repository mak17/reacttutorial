import React, { useState } from 'react';
import Form from './FormComponent';
import { deleteRate } from '../actions/actions';
import { connect } from 'react-redux';

const Element = ({id, author, rate, comment}) => {
    
    const [isVisibleForm, setIsVisibleForm] = useState(false);

    const triggerForm = () => {setIsVisibleForm(!isVisibleForm)};

    const removeComment = () => {
        deleteRate(id);
    }

    const FormOrButton = () => isVisibleForm 
                                ? <Form isNewRate={false}
                                    id={id}
                                    author={author}
                                    rate={rate}
                                    comment={comment}
                                    callback={triggerForm}/> 
                                : <button onClick={triggerForm}>Edit comment</button>

    return (
        <div>
            <p>Author: {author}</p>
            <p>Rate: {rate}</p>
            <p>Comment: {comment}</p>
            <button onClick={removeComment}>Remove Comment</button>
            <FormOrButton />
            -----------------------------------------------------------------------------
        </div>
    )
}

const connectActions = ({
    deleteRate
});

const ElementWrapper = connect(null, connectActions)(Element);

export default ElementWrapper;