import React from 'react';

const InputField = () => {
    const [value, setValue ] = React.useState('');

    const handleOnChange = (event) => setValue(event.target.value)

    const handelOnCLick = () => setValue('');
    return (
        <div>
            <input value={value} placeholder='example message' onChange={handleOnChange} type='text'/>
            <button onClick={handelOnCLick}>Reset</button>
            <h1>{value}</h1>
        </div>
    );
};

export default InputField