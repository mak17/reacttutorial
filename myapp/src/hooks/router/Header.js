import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const Header = () => {

    return (
        <header>
            <nav>
                <li>
                    <NavLink to="/">Main page</NavLink>
                </li>
                <li>
                    <NavLink to="/react">React</NavLink>
                </li>
                <li>
                    <NavLink to="/redux">Redux</NavLink>
                </li>
                <li>
                    <NavLink to="/mbox">MobX</NavLink>
                </li>
                <li>
                    <NavLink to="/typescript">TypeScript</NavLink>
                </li>
            </nav>
        </header>
    )
}
export default Header