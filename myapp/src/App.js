import './App.css'
import React, { Component } from 'react';
import SayHecllo from './sectionThree/sayHello/SayHello';
import Counter from './sectionThree/counter/Counter';
import Sign from './sectionThree/sign/Sign';
import ShowHide from './sectionThree/showHide/ShowHide';
import ShoppingList from './sectionThree/shoppingList/ShoppingList';
import * as Products from './sectionThree/shoppingList/products';
import TaskOne from './sectionThree/inputField/TaskOne';
import Ticket from './sectionFive/ticket/Ticket';
import User from './sectionFive/users/User';
import Shop from './sectionFive/shop/Shop';
import Omen from './sectionFive/omen/Omen';
import Person from './sectionFive/persons/Person';
import ToDoApp from './ToDoApp/ToDoApp';
import Form from './sectionFive/form/Form';
import CurrencyExchange from './sectionFive/currencyExchange/CurrencyExchange';
import { BrowserRouter, Link, Route, NavLink, Switch} from 'react-router-dom';
//import Context from './DrugaCzesc/Context';
import AddSign from './hooks/addSign/AddSign';
import InputField from './hooks/inputField/InputField';
import Ref from './hooks/ref/Ref';
import UseHookRef from './hooks/useRef/UseHookRef';
import UseReducer from './hooks/useReducer/UseReducer';
import UseMemo from './hooks/useMemo/UseMemo';
import UseCallback from './hooks/useCallback/UseCallback';
import UseLayoutEffect from './hooks/useLayoutEffect/UseLayoutEffect';
import RouterComponent from './hooks/router/RouterComponent'
import ReduxComponent from './redux/ReduxComponent'
 
class App extends Component {
  /**
   * Render method is render after each change on state
   */
  render() {
    return (
      <div>
        {
          //sayHello()
        }
        {
          //<SayHecllo nameFromProps="Jack" />
        }
        {
          //<Counter />
        }
        {
          //<Sign />
        }
        {
          //<ShowHide />
        }
        {
          //<TaskOne />
        }
        {
          //<ShoppingList products={Products.getProducts()} />
        }
        {
          //<ToDoApp />
        }
        {
          //<Ticket />
        }
        {
          //<User />
        }
        {
          //<Shop />
        }
        {
          //<Omen />
        }
        {
          //<Person />
        }
        {
          //<Form />
        }
        {
          //<CurrencyExchange />
        }
        {
          /**
           * Routing - handling URL
           * After change the URL is request to server and page is reloaded
           * In SPA we have all page from server. Routing intercepts the request and display new page without reload page
           * We have to use BrowserRouter component
           * We use 'Link' insted of '<a>' and 'to' instead of 'href'
           * To decide which page show we use <Route path="/" exact component={nameOfComponent} />
           * We can use 'NavLink' instead of 'Link'. NavLink add class 'active' to the clicked element
           * Switch recognize when path is different and not fit do declared paths then show for example error page
          */

          /*
          <BrowserRouter>      
            <div>
              <ul>
                <li><NavLink to="/" exact>Page 1</NavLink></li>
                <li><NavLink to="/pageTwo">Page 2</NavLink></li>
                <li><NavLink to="/pageThree">Page 3</NavLink></li>
              </ul>
            </div>
            <section>
              <Switch>
                <Route path="/" exact component={PageOne} />
                <Route path="/pageTwo" exact component={PageTwo} />
                <Route path="/pageThree" exact component={PageThree} />
                <Route component={ErrorPage} />
              </Switch>
            </section>
          </BrowserRouter>
          */
        }
        {
          //<Context />
        }
        {
          //<AddSign />
        }
        {
          //<InputField />
        }
        {
          //<Ref />
        }
        {
          //<UseHookRef />
        }
        {
          //<UseReducer />
        }
        {
          //<UseMemo />
        }
        {
          //<UseCallback />
        }
        {
          //<UseLayoutEffect />
        }
        {
          //<RouterComponent />
        }
        {
          <ReduxComponent />
        }
      </div>
    )
  }
}

export default App;

const sayHello = () => <h1>Hello World!!!</h1>

const PageOne = () => <h1>Page one</h1>;
const PageTwo = () => <h1>Page two</h1>;
const PageThree = () => <h1>Page three</h1>;
const ErrorPage = () => <h1>Error Page</h1>