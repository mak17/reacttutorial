import React, { useCallback, useState } from 'react';
import Counter from './Counter';
import CounterWithHook from './CounterWithHook';

const UseCallback = () => {
    const [firstCounter, setFirstCounter] = useState(0);
    const [secondCounter, setSecondCounter] = useState(0);


    const increaseFirstCounter = useCallback(() => setFirstCounter(prevValue => prevValue + 1), [firstCounter]);
    //const increaseSecondCounter = useCallback(() => setSecondCounter(prevValue => prevValue + 1), [secondCounter]);
    const increaseSecondCounter = useCallback(() => setSecondCounter(prevValue => prevValue + 1), []);

    const CounterWithHookComponent = <CounterWithHook callBack={increaseSecondCounter}/>

    return(
        <div>
           <h1>First counter: {firstCounter}</h1>
           <h1>Second counter: {secondCounter}</h1>
           <Counter callBack={increaseFirstCounter} />
           {CounterWithHookComponent}
        
        </div>
    )
}

export default UseCallback