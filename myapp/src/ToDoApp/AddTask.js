import React, { Component } from 'react';

class AddTask extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            isImportant: false,
            errorMessage: 'This field is required',
            showError: false
        }

        this.handlerNewTask = this.handlerNewTask.bind(this);
        this.addTask = this.addTask.bind(this);
    }

    handlerNewTask(event) {
        this.setState({
            [event.target.name]: event.target.value,
            showError: false
        })
    }

    addTask(event) {
        event.preventDefault()

        if(this.state.name.length > 0) {
            this.props.addTask(this.state.name, this.state.isImportant)
            this.setState({
                name: '',
                isImportant: false
            });
        } else {
            this.setState({
                showError: true
            });
        }
        
    }

    render() {
        const { name, isImportant, showError, errorMessage } = this.state;
        return (
            <div style={{border: "2px solid red"}}>
                <h1>AddTask</h1>
                <form onSubmit={event => this.addTask(event)}>
                    Name: <input type="text" name="name" value={name} onChange={event => this.handlerNewTask(event)}/>{showError && <span style={{color: "red"}}>{errorMessage}</span>}
                    <br/>
                    Is important: <input type="checkbox" name="isImportant" value={isImportant} onChange={event => this.handlerNewTask(event)}/>
                    <br/>
                    <button>Add</button>
                </form>
            </div>
            
        )
    }
}

export default AddTask