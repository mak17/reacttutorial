import React from 'react';
/*
* useMemo checking if props were changed.
* If is change in props the component will be render
* if not component will not change
*/
const Counter = () => {
   console.log('render');

    return(
        <h1>Component is never change</h1>
    )
}

export default  React.memo(Counter)