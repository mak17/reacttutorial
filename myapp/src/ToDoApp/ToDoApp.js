import React, { Component } from 'react';
import TaskList from './TaskList';
import AddTask from './AddTask';

class ToDoApp extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            tasks: [],
            counter: 0
        }

        this.deleteTask = this.deleteTask.bind(this);
        this.finishTask = this.finishTask.bind(this);
        this.addTask = this.addTask.bind(this)
    }

    testData = [
        {id: 0, text: 'Task 1', important: false, active: true},
        {id: 1, text: 'Task 2', important: false, active: true},
        {id: 2, text: 'Task 3', important: true, active: true},
        {id: 3, text: 'Task 4', important: false, active: true},
        {id: 4, text: 'Task 5', important: true, active: true},
        {id: 5, text: 'Task 6', important: false, active: true},
        {id: 6, text: 'Task 7',  important: false, active: true},
        {id: 7, text: 'Task 8', important: false, active: true},
        {id: 8, text: 'Task 9', important: false, active: true},
        {id: 9, text: 'Task 10', important: false, active: true},
    ];

    /**
     * Method is trigger only once after the component is rendered (after render method).
     * In this method we can use fetch method 
     * In this method we can trigger asynchronous operations
     * We should omit change the state in this method
     * We can add here timers
     */
    componentDidMount() {
        this.setState({
            tasks: this.testData,
            counter: this.testData.length})
    }    

    deleteTask(id){
        const tasks = [...this.state.tasks];
        const index = tasks.findIndex(task => task.id === id);
        tasks.splice(index,1)

        this.setState({
            tasks: tasks
        });
    }

    finishTask(id) {
        const tasks = [...this.state.tasks]

        tasks.forEach(task => {
            if(task.id === id)
            task.active = false;
        })

        this.setState({
            tasks: tasks
        })
    }

    addTask(name, isImportant) {
        const newTask = {
            id: this.state.counter,
            text: name,
            important: isImportant,
            active: true
        }

        this.setState({
            tasks: [...this.state.tasks, newTask],
            counter: this.state.counter++
        });

        return true
    }

    render() {
        const { tasks } = this.state
        return (
            <div>
                <h1>ToDo App</h1>
                <AddTask addTask={this.addTask}/>
                <TaskList tasks={tasks} deleteTask={this.deleteTask} finishTask={this.finishTask}/>
            </div>
        )
    }
}

export default ToDoApp