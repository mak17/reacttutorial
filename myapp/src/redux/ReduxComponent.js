import React from 'react';
import { Provider } from 'react-redux';
import store from './reducers/store';
import Rates from './components/Rates';
import FormComponent from './components/FormComponent';

const ReduxComponent = () => {
    
    return (
        <Provider store={store}>
            <FormComponent  isNewRate={true}/>
            <Rates />
        </Provider>
    )
}

export default ReduxComponent