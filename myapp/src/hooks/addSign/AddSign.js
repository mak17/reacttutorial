import React from 'react';

const AddSign = () => {
    /**
     * To create variable we can use useState -> const [counter, setCounter ] = React.useState(3); 
     * useState use to store a state
     * hooks use only in functional components
     * in useStete() we init a value
     * useState retuen two arguments (variable and method to set the variable)
     * Example:
     * const (state, setState) = useState({a: 2, b: 4});
     * display variables: state.a, state.b
     * update one field in object: setState(...state, a: 3)
     */
    const [text, setText ] = React.useState('');

    /**
     * 
     * useEffect(
     *	// first param of function, block of code describe what we want to do
     *	() => {
     *      // this block of code will be trigger after update the state
     *		return () => {
     *          // this method is trigger after unmount component
     *          // works like componentWillUnmount
     *          // here we can clear memory 
     *		}	
     *	},
     *	// second param of function
     *  // here we have dependecy table
     *   // if table is empty useEffect trigger only once
     *  // if we want to trigger useEffect only after one variable we should add this variable to dependency table
     *	[]
     * )
    */
    const handleOnClick = () => setText(`${text}@`)
    return (
        <div>
            <button onClick={handleOnClick}>Add</button>
            <h1>{text}</h1>
        </div>
    );
};

export default AddSign