import React, { Component } from 'react';

const Message = (props) => <h1>{props.message}</h1>

class Ticket extends Component {
    
    state = {
        isConfirmed: false,
        isFormSubmited: false
    }

    handleChange() {
        this.setState({
            isConfirmed: !this.state.isConfirmed,
            isFormSubmited: false
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        if(!this.state.isFormSubmited) {
            this.setState({
                isFormSubmited: true
            });
        }    
    }

    showMessage = () => {
        return <Message message={this.state.isConfirmed ? 'Yes' : 'No'}/>
    }

    render() {
        const { isConfirmed, isFormSubmited } = this.state;

        return (
            <>
                <form onSubmit={this.handleSubmit}>
                    <h1>Buy a ticket</h1>
                    <input id="ticket" type="checkbox" checked={isConfirmed} onChange={this.handleChange.bind(this)}/>
                    <label htmlFor="ticket">Are you 18 years old</label>
                    <br />
                    <button type="submit">Buy</button>
                </form>
                {isFormSubmited && this.showMessage()}
            </>
        )
    };
}

export default Ticket