import { render } from '@testing-library/react';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import store from '../reducers/store';
import { addRate, editRate } from '../actions/actions';
const FormComponent = ({
    isNewRate,
    id, 
    author = '', 
    rate = 0, 
    comment = '', 
    callback,
    addRate,
    editRate
}) => {
    
    const [idInput, setIdInput] = useState(id);
    const [authorInput, setAuthorInput] = useState(author);
    const [commentInput, setCommentInput] = useState(comment);
    const [rateInput, setRateInput] = useState(rate);


    const handleChange = event => {
        switch(event.target.name) {
            case 'id':
                setIdInput(event.target.value);
                break;
            case 'author':
                setAuthorInput(event.target.value);
                break;
            case 'comment':
                setCommentInput(event.target.value);
                break;
            default:
                setRateInput(event.target.value);
        }
    };

    const handleSubmit = event => {
        event.preventDefault();

        const newRate = {
            id: idInput,
            author: authorInput,
            rate: rateInput,
            comment: commentInput,
        }

        if(callback) {
            callback();
        }

        isNewRate ? addRate(newRate) : editRate(newRate);
    };

    return (
        <form onSubmit={handleSubmit}>
            <label>
                Id: 
                <input onChange={handleChange} name='id' type='text' value={idInput} />
            </label>
            <br/>
            <label>
                Author: 
                <input onChange={handleChange} name='author' type='text' value={authorInput} />
            </label>
            <br/>
            <label>
                Comment: 
                <input onChange={handleChange} name='comment' type='text' value={commentInput} />
            </label>
            <br/>
            <label>
                Rate: 
                <input onChange={handleChange} name='rate' type='number' value={rateInput} />
            </label>
           <button type="submit">{id === 100000 ? 'Save' : 'Update'}</button>
        </form>
    )
}

const connectActions = ({
    addRate,
    editRate
});

const FormWrapper = connect(null, connectActions)(FormComponent);

export default FormWrapper;