import React, { useEffect, useLayoutEffect } from 'react';
const UseLayoutEffect = () => {
   
    useEffect(() => console.log("Po wyrenderowaniu"),[]);

    /**
     * useLayoutEffect trigger between rende and useEffect
     * 
     */
    useLayoutEffect(() => console.log("Przed wyrenderowaniem"),[]);

    console.log("render");
    return(
        <div>
           <h1>Hello</h1>
        </div>
    )
}

export default UseLayoutEffect