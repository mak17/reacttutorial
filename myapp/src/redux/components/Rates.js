import React from 'react';
import Element from './Element';
import { connect } from 'react-redux';

const Rates = ({rates}) => {

    console.log(rates);
    return (
        <div>
            { rates.map(rate => <Element key={rate.id} {...rate} />) }
        </div>
    )
}

const connectRedux = store => ({
    rates: store.rates
});

const rateList = connect(connectRedux)(Rates);

export default rateList;