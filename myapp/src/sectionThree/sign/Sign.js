import React, { Component } from 'react';

class Sign extends Component {

    state = {
        text: ''
    };

    addSign = () => {
        const sign = '#';
        this.setState({text: this.state.text + sign});
    }
    
    render () {
        const { text } = this.state;
        return (
            <div>
                <button onClick={this.addSign}> Add *</button>
              <h1>{text}</h1>
            </div>
        )
    }
}

export default Sign