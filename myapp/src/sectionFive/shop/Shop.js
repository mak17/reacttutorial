import React, { Component } from 'react';
import NumberItems from './NumberItems';
import Recipt from './Recipt';
import Items from './Items';

const items = [
    {
        id: 0,
        name: 'Butter',
        price: 20
    },
    {
        id: 1,
        name: 'Ham',
        price: 10
    },
    {
        id: 2,
        name: 'Apple',
        price: 15
    },
    {
        id: 3,
        name: 'Water',
        price: 20
    },
    {
        id: 4,
        name: 'Beer',
        price: 5
    },
];

class Shop extends Component {

    constructor(props) {
        super(props);

        this.state = {
            shoppingList: []
        }

        this.getTotalCost = this.getTotalCost.bind(this);
    }

    getTotalCost() {
        return this.state.shoppingList.reduce((sum, {price}) => sum + price, 0);
    }

    handleShoppingList = (id) => {
        const item = items.find(item => item.id === id);
        let shoppingList = this.state.shoppingList;
        const indexOfItem = shoppingList.indexOf(item);

        indexOfItem > -1 ? shoppingList.splice(indexOfItem,1) : shoppingList.push(item)

        this.setState({
            shoppingList: shoppingList
        });
    }

    isInShoppingList = id => this.state.shoppingList.find(item => item.id === id);

    render() {
        const { shoppingList } = this.state;
        return (
            <>
                <NumberItems number={shoppingList.length} />
                <Recipt cost={this.getTotalCost()} />
                <Items items={items} handleShoppingList={this.handleShoppingList} isInShoppingList={this.isInShoppingList}/>
            </>
        )
    }
}

export default Shop