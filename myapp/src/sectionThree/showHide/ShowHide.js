import React, { Component } from 'react';

class ShowHide extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isActive: false
        };

        /**
         * When we use normal functiom we have to trigger bind() method on our method.
         * To the bind metod we have to sent this.
         * If we don't do this and we try make change on 'this' inner the function
         * we will get error that 'this' is undefind.
         * After bind function we can make change on 'this'
         * bind function return the function on which we trigger the bind() with 'this' object
         * 
         * We don't have to do it with arrow function because of
         * arrow function inherits 'this' object after the parent class
         * 
         */
        this.show = this.show.bind(this);
    }

    /**
     * setState
     * setState is asynchronous
     * setState is add to queue and is run after execut previos
     * in setState is merging methods
     * in result when we have a few setSteto on this same variable to the variable will be assign value from las setState
     * If we want work on actuall state we should use prevState
     *
     * Example:
     * this.setState() {
	 * a: 4,
	 * b: 5
     * }
     *
     * this.setState({
	 * a: 9 // work not on the value 5 but work on the null value
     * })
     * 
     * a -> 9, b->5 
     * 
     */
    show() {
        //debugger
        this.setState({isActive: !this.state.isActive});
    }

    render () {
        const text = 'Example message';
        const { isActive } = this.state;
        return (
            <div>
              <button onClick={this.show}>{isActive ? 'Hide' : 'Show'}</button>
              <h1>{isActive && text}</h1>
              ------------------------
              {isActive ? <h1>{text}</h1> : null}
              -----------------------
              {isActive &&  <h1>{text}</h1>}
            </div>
        )
    }
}

export default ShowHide