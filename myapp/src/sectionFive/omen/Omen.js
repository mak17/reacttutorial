import React, { Component } from 'react';

class Omen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            omens: ['Omen 1','Omen 2','Omen 3'],
            selectedOmen: '',
            newOmen: ''
        }

        this.drawOmen = this.drawOmen.bind(this);
        this.addOmen = this.addOmen.bind(this);
        this.handelChange = this.handelChange.bind(this);
    }

    drawOmen() {
        const omenId = Math.floor(Math.random() * (this.state.omens.length - 0)) + 0;
        this.setState({
            selectedOmen: this.state.omens[omenId] 
        });
    }

    addOmen(event) {
        event.preventDefault()
        let omenList = [...this.state.omens];
        omenList.push(this.state.newOmen);
        this.setState({
            omens: omenList,
            newOmen: '',
            selectedOmen: ''
        });
    }

    handelChange(event) {
        this.setState({
            newOmen: event.target.value
        });
    };

    render() {
        const { selectedOmen, newOmen } = this.state;
        return(
            <>
            <button onClick={this.drawOmen}>Draw omen</button>
            <br />
            <form onSubmit={this.addOmen}>
                <input type="text" name="NewOmen" value={newOmen} onChange={this.handelChange}/>
                <button type="submit">Add omen</button>
            </form>
            
            <br />
            <h1>{selectedOmen}</h1>
            </>
        )
    };
}

export default Omen