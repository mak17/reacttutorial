import React, { Component } from 'react';

class Item extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { item, handleShoppingList, isInShoppingList } = this.props;
        return (
            <div>           
                <input type="checkbox" id={`item_${item.id}`} checked={isInShoppingList(item.id)} onChange={() => handleShoppingList(item.id)} />
                <label htmlFor={`item_${item.id}`}>{`${item.name} - ${item.price}`}</label>
            </div>
        )
    }
}

export default Item