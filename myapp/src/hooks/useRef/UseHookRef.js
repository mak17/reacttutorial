import React, { useRef } from 'react';

/**
* UseRef create reference to the HTML element
* reference creates when component is generated and live until unmount component
* after each render component is create new reference
* based on reference we can influance to element
* We should remove reference when component unmount  
*/
const UseHookRef = () => {

    const inputRef = useRef();

    const handleFocus = () => {
        inputRef.current.focus();
    }

    return(
        <div>
            <input ref={inputRef} type='text' />
            <button onClick={handleFocus}>CLick me</button>
        </div>
    )
}

export default UseHookRef