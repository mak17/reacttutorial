export const ADD_RATE = 'ADD_RATE';
export const DELETE_RATE = 'DELETE_RATE';
export const EDIT_RATE = 'EDIT_RATE';

export const addRate = ({id, author, rate, comment}) => ({   
    type: ADD_RATE,
    payload: {
        id,
        author,
        rate,
        comment,
    }
})

export const deleteRate = id => ({
    type: DELETE_RATE,
    payload: {
        id,
    }
})

export const editRate = ({id, author, rate, comment}) => ({
    type: EDIT_RATE,
    payload: {
        id,
        author,
        rate,
        comment,
    }
})