import React from 'react';

const CounterWithHook = ({callBack}) => {
    console.log("render from second counter");

    return(
        <button onClick={callBack}>Second counter</button>
    )
}

export default React.memo(CounterWithHook)