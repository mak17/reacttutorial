import React, { PureComponent } from 'react';

class Ref extends PureComponent {
    inputText = React.createRef();
    paragrafRef = React.createRef();

    handleFocus = () => {
        this.inputText.current.focus();
    }

    handleParagraf = () => {
        this.paragrafRef.current.textContent += '*';
    }

    render() {
        return (
            <div>
                <input ref={this.inputText}
                type='text' />
                <button onClick={this.handleFocus}>
                    Ustaw fokus
                </button>
                <br />
                <p ref={this.paragrafRef}>Test</p>
                <button onClick={this.handleParagraf}>Clikc me</button>
            </div>
        )
    }
}

export default Ref