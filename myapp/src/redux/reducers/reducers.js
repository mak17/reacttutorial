import { ADD_RATE, DELETE_RATE, EDIT_RATE } from '../actions/actions';
import store from './store';

export const reducer = (state = [], action) => {
    
    switch(action.type) {
        case ADD_RATE:
            return [...state, action.payload];
        case EDIT_RATE:
            return state.map(element => {

                if(element.id !== action.payload.id) {
                    return element;
                }
                
                const { author, rate, comment } = action.payload;
                return {
                    id: element.id,
                    author,
                    rate,
                    comment,
                };
            });
        case DELETE_RATE: 
            return state.filter(element => element.id !== action.payload.id);
        default:
            return state
    }
}