import React, { Component } from 'react';

class TaskOne extends Component {

    state = {
        text: ''
    };

    handleInputChange = event => this.setState({text: event.target.value});
    
    resetText = () => this.setState({text: ''});

    render () {
        const { text } = this.state;
        return (
            <div>
              <input value={text} onChange={this.handleInputChange} type="text" />
              <button onClick={this.resetText}>Reset</button>
              <h1>{text}</h1>
            </div>
        )
    }
}

export default TaskOne