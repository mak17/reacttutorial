import React, { Component } from 'react';
import Task from './Task';
const TaskList = props => {
    
    const activeTasks = props.tasks.filter(task => task.active);
    const noActiveTasks = props.tasks.filter(task => !task.active);

    return (
        <div>
            <h1>To do lisy {activeTasks.length}</h1>
            {activeTasks.length > 0
                ? activeTasks.map(task => <Task key={task.id} task={task} deleteTask={props.deleteTask} finishTask={props.finishTask}/>)
                : <h1>List is empty</h1>}
            ------------------------
            <h1>Tasks to do {noActiveTasks.length}</h1>
            {noActiveTasks.length > 0 
                ? noActiveTasks.map(task => <Task key={task.id} task={task}/>)
                : <h1>Nothig you did</h1>
            }
        </div>
    )
}

export default TaskList