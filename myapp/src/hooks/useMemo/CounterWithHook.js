import React from 'react';

const CounterWithHook = () => {
   console.log('render with hook');

    return(
        <h1>Component with hook is never change</h1>
    )
}

export default CounterWithHook