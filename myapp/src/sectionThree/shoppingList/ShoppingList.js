import React, { Component } from 'react';

class ShoppingList extends Component {
    state = {
        name: 'Shopping list'
    };

    render () {
        const { name } = this.state;
        const { products } = this.props;
        return (
            <div>
                <h1>{name}</h1>
                <ul>
                    {products.map((product, index) => <li key={index}>{product}</li>)}
                </ul>
            </div>
        )
    }
}

export default ShoppingList