import React from 'react';
import { Switch, Route, Redirect, useHistory, useParams } from 'react-router-dom';

const Content = () => {

    const history = useHistory();

    const paramObject = useParams();
    
    const handleOnClick = (path, isActive_) => {
        console.log(history);
        console.log(isActive_)
        const location = {
            pathname: `/${path}`,
            state: {
                isActive: isActive_
            }
        }

        history.push(location);
    }

    const goBack = () => history.goBack();

    const MboxComponent = () => <div>
                                    <h1>Mbox Component</h1>
                                    <button onClick={goBack}>Go back</button>
                                </div>

    const ReactComponent = () => <div>
                                    <h1>React Component</h1>
                                    <button onClick={() =>handleOnClick('redux', true)}>Go to Redux component</button>
                                    <button onClick={goBack}>Go back</button>
                                </div>

    const ReduxComponent = () => <div>
                                    <h1>Redux Component</h1>
                                    {history.location.state?.isActive && <h1>Hello</h1>}
                                    <button onClick={goBack}>Go back</button>
                                </div>

    const TypeScriptComponent = () => <div>
                                        <h1>TypeScript Component</h1>
                                        <button onClick={() =>handleOnClick('redux', false)}>Go to Redux component</button>
                                        <button onClick={goBack}>Go back</button>
                                    </div>

    const MainComponent = () => <div>
                                    <h1>Main Component</h1>
                                    <button onClick={goBack}>Go back</button>
                                </div>

    return (
       <main>
           <Switch>
                <Route path="/mbox" component={MboxComponent}></Route>
                <Route path="/react" component={ReactComponent}></Route>
                <Route path="/redux" component={ReduxComponent}></Route>
                <Route path="/typescript" component={TypeScriptComponent}></Route>
                <Redirect from="*" to="/" />
           </Switch>
       </main>
    )
}
export default Content