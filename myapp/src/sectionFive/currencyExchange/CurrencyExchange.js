import React, { Component } from 'react';

const products = [
    {
        id: 0,
        name: 'oranges',
        unit: 'KG',
        pricePLN: 10

    },
    {
        id: 1,
        name: 'water',
        unit: 'L',
        pricePLN: 20
    },
    {
        id: 2,
        name: 'banana',
        unit: 'PCS',
        pricePLN: 15
    }
];

const curriences = [
    {
        id: 0,
        name: 'PLN',
        ratio: 1
    },
    {
        id: 1,
        name: 'EURO',
        ratio: 4.40
    },
    {
        id: 2,
        name: 'DOLAR',
        ratio: 3.50
    },
];

class CurrencyExchange extends Component {

    constructor(props) {
        super(props)

        this.state = {
            product: 'oranges',
            quantity: 0
        }

        this.changeProduct = this.changeProduct.bind(this);
        this.changeQuantity = this.changeQuantity.bind(this);
        this.getUnit = this.getUnit.bind(this);
        this.getPrice = this.getPrice.bind(this);
    };

    changeProduct(event) {
        this.setState({product: event.target.value});
    };

    changeQuantity(event) {
        this.setState({quantity: event.target.value});
    };

    getUnit() {
        return products.find(product => product.name === this.state.product).unit;
    };

    getPrice(ratio) {
        const product = products.find(product => product.name === this.state.product);
        return (product.pricePLN/ratio*this.state.quantity).toFixed(2);
    };

    render() {
        const { product, quantity } = this.state;
        return (
            <>
               <h1>Select product</h1>
               <select value={product} onChange={this.changeProduct}>
                    {products.map(product => <option key={product.id} value={product.name}>{product.name}</option>)}
               </select>
               <h1>Fill quantity</h1>
               <input value={quantity} type="number" onChange={this.changeQuantity} ></input>{this.getUnit()}
               {curriences.map(currience => <h1 key={currience.id}>{`Price in ${currience.name} is ${this.getPrice(currience.ratio)}`}</h1>)}
            </>
        )
    }
}

export default CurrencyExchange