import React, { Component } from 'react';

const data = {
    users: [
        {
            id: 0,
            age: 22,
            name: 'Marcin',
            sex: 'M'
        },
        {
            id: 1,
            age: 24,
            name: 'Sonia',
            sex: 'F'
        },
        {
            id: 2,
            age: 29,
            name: 'Arek',
            sex: 'M'
        },
        {
            id: 3,
            age: '33',
            name: 'Tomek',
            sex: 'M'
        },
        {
            id: 4,
            age: 33,
            name: 'Aneta',
            sex: 'F'
        }
    ]
}

const UserComponent = ({user}) => <div style={{border: '2px black solid'}}>
                                    <h1>{`Name: ${user.name}`}</h1>
                                    <h1>{`Age: ${user.age}`}</h1>
                                </div>

class User extends Component {

    state = {
        users: data.users,
        option: 'ALL'
    }

    showUsers = () => {
        let users = [];
        
        switch(this.state.option) {
            case 'F':
                users = this.state.users.filter(user => user.sex === 'F');
                break;
            case 'M':
                users = this.state.users.filter(user => user.sex === 'M');
                break;
            default:
                users = this.state.users;
        }
        return users.map(user => <UserComponent key={user.id} user={user}/>);
    }

    selectSex(option) {
        this.setState({
            ...this.state,
            option: option
        })
    }

    render() {
        return (
            <div>
                <button onClick={this.selectSex.bind(this, 'F')}>Show women</button>
                <button onClick={this.selectSex.bind(this, 'M')}>Show men</button>
                <button onClick={this.selectSex.bind(this, 'ALL')}>Show all</button>
                {this.showUsers()}  
            </div>
        )
    }
}

export default User